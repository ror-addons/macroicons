-- vim:sw=4 ts=4
MacroIcons = {}
local macroId = EA_Window_Macro.MACRO_ICONS_ID_BASE
local oldId=macroId
local reset = false

function MacroIcons.OnInitialize()
	MacroIcons.hookMacroSelection = EA_Window_Macro.SelectionIconLButtonDown
	EA_Window_Macro.SelectionIconLButtonDown = MacroIcons.MacroSelection
	WindowSetShowing("IconsScrollbar", false)
	CreateWindow("MacroIcons", true)
	WindowSetParent("MacroIcons", "MacroIconSelectionWindow")
	WindowClearAnchors("MacroIcons")
	WindowAddAnchor("MacroIcons", "topright", "MacroIconSelectionWindow", "topright", -5,35)
	WindowAddAnchor("MacroIcons", "bottomright", "MacroIconSelectionWindow", "bottomleft", -34,-5)
	VerticalScrollbarSetMaxScrollPosition("MacroIconsScrollBar", 807)
	VerticalScrollbarSetScrollPosition("MacroIconsScrollBar", 11)
	WindowRegisterCoreEventHandler("MacroIconSelectionWindow","OnMouseWheel", "MacroIcons.MouseWheel")
	WindowRegisterCoreEventHandler("IconsScrollChild","OnMouseWheel", "MacroIcons.MouseWheel")
	for k=1, EA_Window_Macro.NUM_MACRO_ICONS, 1 do
		WindowRegisterCoreEventHandler("MacroIconSelectionWindowIconSlot"..k,"OnMouseWheel", "MacroIcons.MouseWheel")
	end
end

function MacroIcons.NewTextures()
	if macroId < 1 then macroId = 1 end
	if macroId > 14330 then macroId = 14330 end
	-- 19982
	local tex1, _, _ = GetIconData( macroId + 1 )
	local tex2, _, _ = GetIconData( macroId + EA_Window_Macro.NUM_MACRO_ICONS )
	if tex1 == "icon-00001" and tex2 == "icon-00001" then
		if oldId < macroId then
			macroId = macroId + 18
		else
			macroId = macroId - 18
		end
		MacroIcons.NewTextures()
		return
	end
	oldId = macroId
	for  slot = 1, EA_Window_Macro.NUM_MACRO_ICONS do
		local texture, x, y = GetIconData( macroId + slot )
		DynamicImageSetTexture( "MacroIconSelectionWindowIconSlot"..slot.."IconBase", texture, x, y )
	end
	reset = true
	VerticalScrollbarSetScrollPosition("MacroIconsScrollBar", MacroIcons.GetPage())
	reset = false
end

function MacroIcons.MacroSelection(...)
	MacroIcons.hookMacroSelection(...)
	pcall(MacroIcons.NewIcon)
end

function MacroIcons.NewIcon()
	local slot = WindowGetId(SystemData.ActiveWindow.name)
	EA_Window_Macro.iconNum = macroId + slot
	local texture, x, y = GetIconData( EA_Window_Macro.iconNum )
	DynamicImageSetTexture( "EA_Window_MacroDetailsIconIconBase", texture, x, y )
end

function MacroIcons.GetPage()
	-- Page range 0-796
	return math.floor((macroId-2)/18)
end

function MacroIcons.ScrollPos()
	if reset then return end
	local page = math.floor(VerticalScrollbarGetScrollPosition("MacroIconsScrollBar"))
	macroId = (page)*18+2
	MacroIcons.NewTextures()
end

function MacroIcons.MouseWheel(x, y, delta, flags)
    if (delta > 0) then
        macroId = macroId - 18
    elseif (delta < 0) then
       macroId = macroId + 18
    end
    MacroIcons.NewTextures()
end

