<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >	
  <UiMod name="MacroIcons" version="1.2.1" date="1/31/2009" >		
    <Author name="Thurwell" email="" />		    
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />  		
    <Description text="More macro icons"/>		
    <Files>			
      <File name="MacroIcons.lua"/>			
      <File name="MacroIcons.xml"/>		
    </Files>	 		
    <Dependencies>         			
      <Dependency name="EA_MacroWindow"/>		
    </Dependencies>		
    <OnInitialize>			
      <CallFunction name="MacroIcons.OnInitialize" />		
    </OnInitialize>	
  </UiMod>
</ModuleFile>